package main


import ( 
		"time"
		"net/http"
		"fmt"
)

func main() {

	duration, _ := time.ParseDuration("1000ns")

	server := &http.Server{
			Addr       : "172.22.51.15:8092",
			IdleTimeout: duration,
		//	Handler    : 
		}

	http.HandleFunc("/hello", hello)

	server.ListenAndServe()
}


func hello(w http.ResponseWriter, r *http.Request){
	w.Write([]byte("Hello World"))
	return
}
